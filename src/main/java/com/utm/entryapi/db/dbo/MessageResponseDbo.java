package com.utm.entryapi.db.dbo;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Data
@Document(collection = "responses")
public class MessageResponseDbo {
    @Id
    String id;
    String messageId;
    String response;
}
