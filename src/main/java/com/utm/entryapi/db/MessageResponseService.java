package com.utm.entryapi.db;

import com.utm.entryapi.db.dbo.MessageResponseDbo;
import com.utm.entryapi.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageResponseService {

    @Autowired
    MongoTemplate mongoTemplate;

    public MessageResponseDbo getMessage(String messageId) {
        List<MessageResponseDbo> responses = mongoTemplate.find(Query.query(Criteria.where("messageId").is(messageId)), MessageResponseDbo.class);
        if (responses.isEmpty()) {
            throw new NotFoundException();
        }
        return responses.get(0);
    }
}
