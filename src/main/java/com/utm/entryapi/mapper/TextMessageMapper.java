package com.utm.entryapi.mapper;

import com.utm.entryapi.dto.api.ClientIdDto;
import com.utm.entryapi.dto.api.TextRequestDto;
import com.utm.entryapi.dto.messages.KafkaFileMessageDto;
import com.utm.entryapi.dto.messages.KafkaTextMessageDto;
import com.utm.entryapi.dto.messages.MessageTypeEnum;
import org.mapstruct.Mapper;

@Mapper
public interface TextMessageMapper {
    KafkaTextMessageDto requestDtoToKafkaTextMessage(TextRequestDto textRequestDto, String id);

    KafkaFileMessageDto requestDtoToKafkaFileMessage(ClientIdDto clientId, String id, byte[] file, MessageTypeEnum messageType);


}
