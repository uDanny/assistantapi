package com.utm.entryapi.service.producer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.utm.entryapi.dto.api.ClientIdDto;
import com.utm.entryapi.dto.api.ResponseDto;
import com.utm.entryapi.dto.api.TextRequestDto;
import com.utm.entryapi.dto.messages.KafkaFileMessageDto;
import com.utm.entryapi.dto.messages.KafkaTextMessageDto;
import com.utm.entryapi.mapper.TextMessageMapper;
import com.utm.entryapi.service.ExtensionResolver;
import com.utm.entryapi.service.IdGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class ProducerService {

    @Value("${kafka.text-topic-name}")
    private String textTopic;

    @Value("${kafka.file-topic-name}")
    private String fileTopic;

    @Value("${kafka.async-enabled}")
    private boolean isAsyncEnabled;

    private final IdGenerator idGenerator;
    private final TextMessageMapper textMessageMapper;
    private final KafkaTemplate<String, JsonNode> kafkaTemplate;
    private final ExtensionResolver extensionResolver;
    private final ObjectMapper objectMapper = new ObjectMapper();


    @Autowired
    public ProducerService(IdGenerator idGenerator, TextMessageMapper textMessageMapper, KafkaTemplate<String, JsonNode> kafkaTemplate, ExtensionResolver extensionResolver) {
        this.idGenerator = idGenerator;
        this.textMessageMapper = textMessageMapper;
        this.kafkaTemplate = kafkaTemplate;
        this.extensionResolver = extensionResolver;
    }

    public ResponseDto performAction(TextRequestDto textRequestDto) {

        String id = idGenerator.getId(textRequestDto.getClientId());

        KafkaTextMessageDto message = textMessageMapper.requestDtoToKafkaTextMessage(textRequestDto, id);
        JsonNode jsonNode = objectMapper.convertValue(message, JsonNode.class);

        if (isAsyncEnabled) {
            sendAsync(id, message.toString(), jsonNode, textTopic);
        } else {
            kafkaTemplate.send(textTopic, id, jsonNode);
        }
        return new ResponseDto(id);

    }


    public ResponseDto performAction(MultipartFile file, ClientIdDto clientIdDto) throws IOException {
        String id = idGenerator.getId(clientIdDto.getClientId());
        KafkaFileMessageDto message =
                textMessageMapper.requestDtoToKafkaFileMessage(clientIdDto, id, file.getBytes(),
                        extensionResolver.checkAndGetExtension(file));
        JsonNode jsonNode = new ObjectMapper().convertValue(message, JsonNode.class);

        if (isAsyncEnabled) {
            sendAsync(id, message.toString(), jsonNode, fileTopic);
        } else {
            kafkaTemplate.send(fileTopic, id, jsonNode);
        }
        return new ResponseDto(id);
    }

    private void sendAsync(String id, String message, JsonNode jsonNode, String topic) {
        ListenableFuture<SendResult<String, JsonNode>> future = kafkaTemplate.send(topic, id, jsonNode);
        future.addCallback(new ListenableFutureCallback<SendResult<String, JsonNode>>() {

            @Override
            public void onSuccess(final SendResult<String, JsonNode> message) {
                System.out.println("sent message= " + message + " with offset= " + message.getRecordMetadata().offset());
            }

            @Override
            public void onFailure(final Throwable throwable) {
                System.out.println("unable to send message= " + message + throwable);
            }
        });
    }
}
