package com.utm.entryapi.service.producer;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.connect.json.JsonSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Properties;
import java.util.stream.Collectors;

@Service
public class ProducerBuilder {

    @Value("${kafka.brokers}")
    String brokers;
    @Value("${kafka.producer-id}")
    String producerId;

    private Properties props = new Properties();

    @PostConstruct
    public void setProps() {
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, brokers);
        props.put(ProducerConfig.CLIENT_ID_CONFIG, producerId);
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class.getName());
        props.put("useNativeEncoding", true);
        props.put("headerMode", "raw");
    }

    public KafkaProducer<String, JsonNode> getTextProducer() {
        return new KafkaProducer<>(props);
    }


    public ProducerFactory<String, JsonNode> getProducerFactory() {
        return new DefaultKafkaProducerFactory<>(
                props.entrySet().stream().collect(
                        Collectors.toMap(
                                e -> e.getKey().toString(),
                                e -> e.getValue().toString()
                        )
                ));
    }
}
