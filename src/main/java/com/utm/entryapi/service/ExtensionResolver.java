package com.utm.entryapi.service;

import com.google.common.collect.ImmutableMap;
import com.utm.entryapi.dto.messages.MessageTypeEnum;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class ExtensionResolver {

    private Map<MessageTypeEnum, List<String>> allowedFormats = ImmutableMap.of(
            MessageTypeEnum.IMAGE, Arrays.asList("image/jpeg", "image/png"),
            MessageTypeEnum.VOICE, Arrays.asList("audio/mpeg")
    );


    public MessageTypeEnum checkAndGetExtension(MultipartFile multipartFile) {
        for (Map.Entry<MessageTypeEnum, List<String>> messageTypeEnumListEntry : allowedFormats.entrySet()) {
            for (String s : messageTypeEnumListEntry.getValue()) {
                if (s.equals(multipartFile.getContentType())) {
                    return messageTypeEnumListEntry.getKey();
                }
            }
        }
        throw new IllegalArgumentException("Unsupported file type");
    }

}
