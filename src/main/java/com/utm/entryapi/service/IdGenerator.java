package com.utm.entryapi.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class IdGenerator {

    private static int COUNTER;

    @Value("${kafka.producer-id}")
    private String producerID;

    public String getId(String clientID) {
        return producerID + clientID + new Timestamp(System.currentTimeMillis()).toString() + COUNTER++;
    }
}
