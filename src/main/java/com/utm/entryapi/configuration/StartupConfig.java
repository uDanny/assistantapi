package com.utm.entryapi.configuration;

import com.fasterxml.jackson.databind.JsonNode;
import com.utm.entryapi.service.producer.ProducerBuilder;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.EnableAsync;

@Configuration
@EnableAsync
public class StartupConfig {
    @Autowired
    private ProducerBuilder producerBuilder;

    @Bean
    KafkaProducer<String, JsonNode> createTextKafkaProducer() {
        return producerBuilder.getTextProducer();
    }


    @Bean
    public KafkaTemplate<String, JsonNode> kafkaTemplate() {
        return new KafkaTemplate<>(producerBuilder.getProducerFactory());
    }

}
