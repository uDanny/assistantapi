package com.utm.entryapi.configuration;

import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories
public class MongoConfig extends AbstractMongoConfiguration {
    @Value("${mongo.db.name}")
    private String dbName;
    @Value("${mongo.db.host}")
    private String dbHost;
    @Value("${mongo.db.port}")
    private int dbPort;

    @Override
    protected String getDatabaseName() {
        return dbName;
    }

    @Override
    public MongoClient mongoClient() {
        return new MongoClient(dbHost, dbPort);
    }

}

