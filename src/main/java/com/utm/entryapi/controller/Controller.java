package com.utm.entryapi.controller;

import com.utm.entryapi.db.MessageResponseService;
import com.utm.entryapi.db.dbo.MessageResponseDbo;
import com.utm.entryapi.dto.api.ClientIdDto;
import com.utm.entryapi.dto.api.ResponseDto;
import com.utm.entryapi.dto.api.TextRequestDto;
import com.utm.entryapi.service.producer.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("api")
public class Controller {
    @Autowired
    private ProducerService producerService;

    @Autowired
    private MessageResponseService messageResponseService;

    @GetMapping("/isAlive")
    public String isAlive() {
        return "OK";
    }

    @PostMapping("/input/text")
    public ResponseEntity<ResponseDto> textRequest(@RequestBody TextRequestDto textRequestDto) {
        return new ResponseEntity<>(producerService.performAction(textRequestDto), HttpStatus.OK);
    }

    @PostMapping("/input/file")
    public ResponseEntity<ResponseDto> imgRequest(@RequestBody MultipartFile file, String clientId) throws IOException {
        return new ResponseEntity<>(producerService.performAction(file, new ClientIdDto(clientId)), HttpStatus.OK);

    }

    @GetMapping("/response/{responseId}")
    public ResponseEntity<MessageResponseDbo> imgRequest(@PathVariable String responseId) {
        return new ResponseEntity<>(messageResponseService.getMessage(responseId), HttpStatus.OK);

    }
}
