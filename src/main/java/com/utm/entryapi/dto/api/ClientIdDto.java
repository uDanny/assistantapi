package com.utm.entryapi.dto.api;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClientIdDto {
    private String clientId;
}
