package com.utm.entryapi.dto.api;

import lombok.Data;

@Data
public class TextRequestDto extends ClientIdDto {
    private String message;

    public TextRequestDto(String clientId, String message) {
        super(clientId);
        this.message = message;
    }
}
