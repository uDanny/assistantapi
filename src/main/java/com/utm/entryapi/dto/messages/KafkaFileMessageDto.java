package com.utm.entryapi.dto.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
public class KafkaFileMessageDto implements Serializable {
    private String id;
    private String clientId;
    private byte[] file;
    private MessageTypeEnum messageType;
}
