package com.utm.entryapi.dto.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
public class KafkaTextMessageDto implements Serializable {
    private String clientId;
    private String message;
    private String id;
}
