package com.utm.entryapi.dto.messages;

public enum MessageTypeEnum {
    IMAGE,
    VOICE
}
