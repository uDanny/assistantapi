# assistantApi

Spring REST service with kafka producer role


# Info

App runs on 8081 port.
Use all defaults Kafka ports (2181 for zookeeper and 9092 for broker), as well as Mongo's (27017).
By default uses synchronous calls for Kafka, it can be changed in swagger.yaml file, as well as other basic configs.
Support file formats: .mp3, .jpg, .png

Communication through Kafka DTOs: KafkaTextMessageDto, KafkaFileMessageDto.

Useful links:

[Postman REST Collection link](https://www.getpostman.com/collections/af111ac120cfff273de5)

[Consumer Example](https://gitlab.com/uDanny/consumerapiexample)


## Kafka running on one node
```bash
zookeeper-server-start /usr/local/etc/kafka/zookeeper.properties

kafka-server-start /usr/local/etc/kafka/server.properties

kafka-topics --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic textTopic

kafka-topics --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic fileTopic

```

## Mongo data insert to be consumed by rest
```bash
mongod config /usr/local/etc/mongod.conf
db.responses.insertOne(    { messageId: "0kd1", response: "firstResponse"} )
```

## Installation

```bash
mvn clean install
mvn spring-boot:run
```
